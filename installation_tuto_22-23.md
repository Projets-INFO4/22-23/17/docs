# **<u>INSTALLATION TUTORIAL - 2022/2023</u>**

# Start oar-docker-compose

   * Cloning these repositories
   ```UNIX
   git clone https://github.com/oar-team/oar-docker-compose
   cd oar-docker-compose/dev
   git clone https://github.com/oar-team/oar3
   ```
   * In `oar-docker-compose/dev/.env_oar_provisoning.sh` make sure to have the following content:
      * SRC is relative to the dev folder
   ```bash
   SRC="oar3"
   ```

   * get-poetry.py has been removed, so you will replace the **following line** in `dev/Dockerfile`:
   ```bash
   RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -
   ```
   with **this one**:
   ```bash
   RUN curl -sSL https://install.python-poetry.org/ | python3 -

   ```

   * Cloning this repository:
   
      - **our project (2022/2023)**

      ```UNIX
      git clone https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/17/projet-dashboard
      cd projet-dashboard/dashboard
      ```

      **OR**

      - previous project (2021/2022)

      ```UNIX
      git clone https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/17/docs
      cd docs/dashboard
      ```
   
   * Launch docker-composer
   ```UNIX
   docker-compose up --build --scale node=2

   ```
   :warning: If you have a problem with Python-Poetry, remove the line you add previously **(third dot) without** rewrite old line. And install Python-Poetry with the **following line**:
   ```UNIX
   curl -sSL https://install.python-poetry.org | python3 -
   ```
   
   `Normally`, you have in **your** terminal these lines (similar):
   
   ```UNIX
   pgadmin     | [2023-01-16 18:59:33 +0000] [1] [INFO] Starting gunicorn 20.1.0
   pgadmin     | [2023-01-16 18:59:33 +0000] [1] [INFO] Listening at: http://[::]:80 (1)
   pgadmin     | [2023-01-16 18:59:33 +0000] [1] [INFO] Using worker: gthread
   pgadmin     | [2023-01-16 18:59:33 +0000] [80] [INFO] Booting worker with pid: 80
   ```

   `Without` lines with **exited with code 255** like this (dev_frontend_1 is an example):
   ```UNIX
   dev_frontend_1 exited with code 255
   ```
   If you have `code 255` problem:
   
   ```TXT
   First of all, I used the following command to access to grub:
   sudo nano /etc/default/grub

   Next, I located the line that starts with GRUB_CMDLINE_LINUX_DEFAULT= and added the following parameter:
   systemd.unified_cgroup_hierarchy=0

   Then, I saved the file, and I updated grub:
   sudo update-grub

   Finally, I reboot my system to apply the changes.
   ```

   If you have other problem, see [github issue](https://github.com/oar-team/oar-docker-compose/issues/).

   For next, open an other terminal.

# Install js dependencies / yarn

   * install yarn on debian/ubuntu

   ```UNIX
   curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
   echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
   sudo apt update
   sudo apt install yarn
   ```

# Find the frontend ip

   ```UNIX
   docker inspect dev_frontend_1
   ```

   * Found IP address (example: 172.23.0.5) and change the proxy url in file `package.json` for the docker address and the port 6668 (e.g 172.23.0.5:6668).

# Start the API

   ```UNIX
   docker exec --user oar --env OARCONFFILE=/etc/oar/oar.conf dev_frontend_1 uvicorn oar.api.app:app --port 8001 --host 0.0.0.0
   ```

   For next, open an other terminal, again.

# Start the dashboard. 

   :warning: At this moment, you have three terminals open.

   ```UNIX
   yarn install
   yarn start
   ```

   A browser should open at url `localhost:3000`.

   The username/password to submit job is `docker`/`docker`. 

## After doing that you might have an error ! Please follow these steps :

1. Type in your shell:

   ```UNIX
   yarn add http-proxy-middleware
   ```
   
2. Delete the line `"proxy": "http://________:6668"` in your `package.json` file.

3. Create a new file named `setupProxy.js` in the src folder.

4. copy and paste those lines in the file:

      ```JS
      const { createProxyMiddleware } = require('http-proxy-middleware');

      module.exports = function(app) {
         app.use(
            '/api',
            createProxyMiddleware({
               target: 'http://________:6668',
               changeOrigin: true,
            })
         );
      };
      ```

5. Then, delete the `node_modules` folder and the `yarn.lock` file.

6. Go back on your shell and do again:

   ```UNIX
   yarn install
   yarn start
   ```

   cf: [forum stackoverflow](https://stackoverflow.com/questions/70374005/invalid-options-object-dev-server-has-been-initialized-using-an-options-object)

# For all other uses after the installation is done.

## Go to your dashboard directory :

   ```UNIX
   dashboard$ docker-compose up
   ```

   After this let it run without touching it.

## In another terminal (with the same path), launch connection with the API :
   (make sure you complete the tuto at this page : https://github.com/oar-team/oar-docker-compose#start-fastapi)

   ```UNIX
   dashboard$ docker exec --user oar --env OARCONFFILE=/etc/oar/oar.conf dev_frontend_1 uvicorn oar.api.app:app --port 8001 --host 0.0.0.0
   ```

   After this let it run without touching it.

### :warning: In case of probleme with the API (cors policy problem in the console): :warning:

   You have to install an extension on your browser to allow the application to access data from another site (your API).

   (PS: I used the "Moesif Origin & CORS Changer" extension on Chrome Browser and it worked well.)

## In a third terminal (still the same path), launch the app with yarn :

   ```UNIX
   dashboard$ yarn start
   ```

   After this let it run without touching it, the app will open itself in your browser
